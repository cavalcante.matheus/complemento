#!/bin/bash


arquivos_1=$(wc -l < "$1")
arquivos_2=$(wc -l < "$2")
arquivos_3=$(wc -l < "$3")
arquivos_4=$(wc -l < "$4")

numero_de_linhas=$arquivos_1
arquivo_com_mais_linhas="$1"

if [ "$arquivos_2" -gt "$numero_de_linhas" ]; then
  numero_de_linhas=$arquivos_2
  arquivo_com_mais_linhas="$2"
fi

if [ "$arquivos_3" -gt "$numero_de_linhas" ]; then
  numero_de_linhas=$arquivos_3
  arquivo_com_mais_linhas="$3"
fi

if [ "$arquivos_4" -gt "$numero_de_linhas" ]; then
  numero_de_linhas=$arquivos_4
  arquivo_com_mais_linhas="$4"
fi

cat $arquivo_com_mais_linhas

